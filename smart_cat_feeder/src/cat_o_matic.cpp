#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <NTPClient.h>
#include <WiFiUdp.h>
#include <Servo.h>

#define NB_TRYWIFI        10    // Nbr de tentatives de connexion au réseau WiFi - Number of try to connect to WiFi network

const char* ssid = "GuyLily";
const char* password = "Patate321";

String time_var;

Servo feeding_gate_servo;

void feed_cat();

WiFiClient espClient;
WiFiUDP ntpUDP;

NTPClient timeClient(ntpUDP, "pool.ntp.org", -14400, 60000);

void setup() {

  feeding_gate_servo.attach(D4);
  feeding_gate_servo.write(0);
  
  Serial.begin(115200);
  Serial.println("");
  Serial.print("Startup reason:");Serial.println(ESP.getResetReason());

  WiFi.begin(ssid, password);

  Serial.println("Connecting to WiFi.");
  int _try = 0;
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print("..");
    delay(500);
    _try++;
    if ( _try >= NB_TRYWIFI ) {
        Serial.println("Impossible to connect WiFi network, go to deep sleep");
        ESP.deepSleep(10e6);
    }
  }
  Serial.println("Connected to the WiFi network");
  
  // Démarrage du client NTP - Start NTP client
  timeClient.begin();
}

void loop() {
    // Met à jour l'heure toutes les 10 secondes - update time every 10 secondes
    timeClient.update();
    time_var = timeClient.getFormattedTime();
    
    if (time_var == "05:00:00" || time_var == "12:00:00" || time_var == "17:00:00" || time_var == "22:00:00")
    {
      feed_cat();
    }
    
    delay(1000);
}

void feed_cat()
{
  Serial.println("Feeding cat");
  feeding_gate_servo.write(90);
  delay(500);
  feeding_gate_servo.write(0);
}
